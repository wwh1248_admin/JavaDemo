package bigData;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;

class ShoppingCar{
    private String name;
    private double price;
    private int amount;

    public ShoppingCar(String name, double price, int amount) {
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
public class bigDataTest2 {
    public static void main(String[] args){
        List<ShoppingCar> list = new ArrayList<ShoppingCar>();
        list.add(new ShoppingCar("java",80,10));
        list.add(new ShoppingCar("php",40,20));
        list.add(new ShoppingCar("python",20,5));
        list.add(new ShoppingCar("C++",60,8));

        // 需求1：求出每样商品总价
        /*list.stream().map((shoppingCar) -> shoppingCar.getPrice() * shoppingCar.getAmount())
                .forEach(System.out::println);*/

        // 需求2：求总价
        /*double totalPrice = list.stream().map((shoppingCar) -> shoppingCar.getPrice() * shoppingCar.getAmount())
                .reduce((sum,m)->sum + m).get();
        System.out.println(totalPrice);*/

        DoubleSummaryStatistics statistics = list.stream().mapToDouble((sc)->sc.getAmount() * sc.getPrice()).summaryStatistics();
        System.out.println("商品样数：" + statistics.getCount());
        System.out.println("最低花费：" + statistics.getMin());
        System.out.println("最高花费：" + statistics.getMax());
        System.out.println("平均花费：" + statistics.getAverage());
        System.out.println("总花费：  " + statistics.getSum());
    }
}
