package bigData;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class bigDataTest {
    public static void main(String[] args){
        List<String> list = new ArrayList<>();
        list.add("java");
        list.add("Java");
        list.add("python");
        list.add("php");
        list.add("php");
        list.add("JavaScript");
        list.add("html");

//        list.forEach(System.out:: println);  //消费型接口

        Stream<String> stream = list.stream(); //取得stream数据流
//        System.out.println(stream.distinct().count());  //去重
//        System.out.println(stream.distinct().collect(Collectors.toList())); //收集，collect泛型

//        List<String> newList = stream.filter((v) -> v.contains("j")).collect(Collectors.toList());  //过滤
        /*List<String> newList = stream.map((item) -> item.toLowerCase()) //map功能型接口
                .filter((v) -> v.contains("j"))
                .collect(Collectors.toList());*/
//        List<String> newList = stream.skip(0).limit(3).collect(Collectors.toList());  //分页
//        newList.forEach(System.out:: println);

        // or and 匹配
        Predicate<String> p1 = (x)->x.contains("Java");
        Predicate<String> p2 = (x)->x.contains("Script1");
        /*if(stream.anyMatch((item) -> item.contains("java"))){
            System.out.println(1);
        }*/
        /*if(stream.anyMatch(p1.and(p2))){
            System.out.println("and");
        }*/
        if(stream.anyMatch(p1.or(p2))){
            System.out.println("or");
        }

    }
}
